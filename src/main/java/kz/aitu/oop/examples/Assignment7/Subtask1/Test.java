package kz.aitu.oop.examples.Assignment7.Subtask1;


public class Test {
    public static void main(String[] args) {
        Shape s1 = new Circle(5.5, "red", false); // Upcast Circle to Shape
        System.out.println(s1); // Circle's toString()
        System.out.println(s1.getArea()); // Circle's getArea()
        System.out.println(s1.getPerimeter()); // Circle's getPerimeter(
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());

        System.out.println(s1.getRadius()); //Trigger, because it is trying to call a method of subclass from superclass reference.
        /* if (s1 instanceof Circle) {
            ((Circle) s1).getRadius();} */

        Circle c1 = (Circle) s1; // Downcast back to Circle
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());

        Shape s2 = new Shape(); // Trigger, because Abstract class is restricted to create objects
        Shape s3 = new Rectangle(1.0, 2.0, "red", false); // Upcast
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());

        System.out.println(s3.getLength());// Trigger, because it is trying to call a method of subclass from superclass reference.


        Rectangle r1 = (Rectangle) s3; // downcast : Create the object that used methods of Rectangle s3
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());

        Shape s4 = new Square(6.6); // Upcast Create the object with sub.subclass's methods
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
        System.out.println(s4.getSide()); // Trigger, because it is trying to call a method of sub.subclass from super.superclass reference.

// Take note that we downcast Shape s4 to Rectangle,
// which is a superclass of Square, instead of Square

        Rectangle r2 = (Rectangle) s4;
        System.out.println(r2); // Rectangle's toString()
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        System.out.println(r2.getSide()); // Trigger, because it is trying to call a method of subclass from superclass reference.

        System.out.println(r2.getLength()); //Rectangle's getLength()
// Downcast Rectangle r2 to Square

        Square sq1 = (Square) r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());
    }
}
