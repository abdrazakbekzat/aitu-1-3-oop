package kz.aitu.oop.examples.Assignment7.Subtask2;

public interface Movable
{
    public void moveLeft();
    public void moveRight();
    public void moveDown();
    public void moveUp();


}
