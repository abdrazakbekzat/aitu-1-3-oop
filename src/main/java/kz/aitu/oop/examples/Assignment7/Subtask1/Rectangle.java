package kz.aitu.oop.examples.Assignment7.Subtask1;

public class Rectangle extends Shape
{
    protected double width;
    protected double length;

    public Rectangle(){
        this.width=1.0;
        this.length=1.0;
    }
    public Rectangle(double width,double length)
    {
        this.width=width;
        this.length=length;
    }
    public Rectangle(double width,double length, String color, boolean filled)
    {
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getLength()
    {
        return length;
    }

    public void setLength(double length)
    {
        this.length = length;
    }

    public double getWidth()
    {
        return width;
    }

    public void setWidth(double width)
    {
        this.width = width;
    }

    @Override
    public double getPerimeter()
    {
        return (width+length)*2;
    }

    @Override
    public double getArea()
    {
        return length*width;
    }

    @Override
    public String toString() {
        return width+length+super.toString();
    }
}
