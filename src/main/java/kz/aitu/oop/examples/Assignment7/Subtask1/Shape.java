package kz.aitu.oop.examples.Assignment7.Subtask1;

public abstract class Shape
{
    protected String color;
    protected boolean filled;

    public Shape (){
        this.color = "red";
        this.filled = true;
    }
    public Shape(String c, boolean f)
    {
        this.color = c;
        this.filled = f;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public boolean isFilled()
    {
        return filled;
    }

    public void setFilled(boolean filled)
    {
        this.filled = filled;
    }


    public String toString()
    {
        return color +filled;
    }
    public abstract double getArea();
    public abstract double getPerimeter();
}
