package kz.aitu.oop.examples.Assignment7.Subtask3;

public class Circle implements GeometricObject
{
    protected double radius = 1.0;

    public Circle(double radius)
    {
        this.radius = radius;
    }

    public String toString()
    {
        return "Circle {radius ="+radius+"}";
    }

    @Override
    public double getPerimeter()
    {
        return 2*radius*Math.PI;
    }

    @Override
    public double getArea()
    {
        return Math.PI*radius*radius;
    }


}
