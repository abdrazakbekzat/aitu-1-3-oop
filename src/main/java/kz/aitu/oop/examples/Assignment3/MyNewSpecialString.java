package kz.aitu.oop.examples.Assignment3;

public class MyNewSpecialString {
    String[] values;


    public MyNewSpecialString(String[] values) {
        this.values = values;
    }

    public int length(){
        return values.length;
    }

    public String valueAt(int position){
        return values[position];
    }

    public boolean contains(String value){
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                return true;
            }
        }
        return false;
    }
    public int count(String value){
        int counter = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                counter++;
            }
        }
        return counter;
    }

    public void Show(){
        for(int i=0; i < values.length; i++){
            System.out.println(values[i]);
        }
    }

}
