package kz.aitu.oop.examples.Assignment3;

public class MyString {
    int[] values;

    public MyString(int[] values) {
        this.values = values;
    }

    public int Length() {
        int length = values.length;
        return length;
    }

    public int valueAt(int position) {
        for(int i : values)
            if (i == position)
                return values[i];
        return -1;
    }

    public boolean contains(int value) {
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                return true;
            }
        }
        return false;
    }

    public int count(int value){
        int counter = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                counter++;
            }
        }
        return counter;
    }

    public void Show(){
        for(int i=0; i < values.length; i++){
            System.out.println(values[i]);
        }
    }
}
