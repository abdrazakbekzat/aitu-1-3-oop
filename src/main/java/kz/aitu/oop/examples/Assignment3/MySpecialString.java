package kz.aitu.oop.examples.Assignment3;

import java.util.HashSet;

public class MySpecialString {
    int[] values;

    public MySpecialString(int[] array){
        this.values=array;
    }

    public int length(){
        return values.length;
    }


    public int valueAt(int position){
        if(values[position] == '\0'){
            return -1;
        }
        return values[position];
    }

    public boolean contains(int value){
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                return true;
            }
        }
        return false;
    }

    public int count(int value){
        int counter = 0;
        for(int i=0; i < values.length; i++){
            if(values[i] == value){
                counter++;
            }
        }
        return counter;
    }

    public void Show(){
        for(int i=0; i < values.length; i++){
            System.out.println(values[i]);
        }
    }
}

