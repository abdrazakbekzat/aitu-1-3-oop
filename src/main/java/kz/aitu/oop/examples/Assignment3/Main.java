package kz.aitu.oop.examples.Assignment3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int[] arr = {1,2,3,4,5,6,7,8,9};
        MyString object = new MyString(arr);

        System.out.println(object.contains(4));
        System.out.println(object.valueAt(4));
        System.out.println(object.count(6));
        object.Show();

        System.out.println("Please write name of your file:");
        String name = sc.nextLine();
        String path = "C:\\Users\\Bekzat\\Desktop\\" + name + ".txt";
        File file = new File(path);
        FileWriter writer = new FileWriter(file);

        writer.close();

        Scanner sc2 = new Scanner(file);
        Scanner sizesc = new Scanner(file);
        int s = 0;
        while(sizesc.hasNextInt()){
            System.out.print(sizesc.nextInt() + " ");
            s++;
        }
        System.out.println();

        int[] arr2 = new int[s];
        int j = 0;
        while(sc2.hasNextInt()){
            arr2[j] = sc2.nextInt();
            j++;
        }
        String[] arrayS = {"Beka", "OOP", "WEB", "Discrete"};
        MyString object2 = new MyString(arr2);
        object2.Show();

        MySpecialString object3 = new MySpecialString(arr);
        System.out.println(object3.length());
        System.out.println(object3.contains(7));
        System.out.println(object3.valueAt(3));
        System.out.println(object3.count(4));
        object.Show();

        MyNewSpecialString objectStr = new MyNewSpecialString(arrayS);
        System.out.println(objectStr.length());
        System.out.println(objectStr.contains("Beka"));
        System.out.println(objectStr.valueAt(6));
        System.out.println(objectStr.valueAt(-1));
        System.out.println(objectStr.count("WEB"));
        objectStr.Show();
    }
}
