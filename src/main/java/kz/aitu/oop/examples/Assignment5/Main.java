package kz.aitu.oop.examples.Assignment5;

public class Main
{
    public static void main(String[] args)
    {
        Square square = new Square(4.0,"red",true);
        System.out.println(square.toString());

        Circle circle = new Circle(7.0,"blue",true);
        System.out.println(circle.toString());

        Rectangle rectangle = new Rectangle(3.0,3.0,"Orange",false);
        System.out.println(rectangle.toString());

        Shape shape = new Shape("red",true);
        shape.setColor("Yellow");
        shape.setFilled(false);
        System.out.println(shape.toString());
    }
}
