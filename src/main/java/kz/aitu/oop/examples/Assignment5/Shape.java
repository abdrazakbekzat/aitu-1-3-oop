package kz.aitu.oop.examples.Assignment5;

public class Shape
{
    private String color;
    private boolean filled;
    public Shape ()
    {
        this.color = "green";
        this.filled = true;
    }
    public Shape(String c, boolean f)
    {
        this.color = c;
        this.filled = f;
    }

    public boolean isFilled()
    {
        return filled;
    }

    public void setFilled(boolean filled)
    {
        this.filled = filled;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    @Override
    public String toString()
    {
        return "Shape{" + "color='" + color + '\'' + ", filled=" + filled + '}';
    }
}
