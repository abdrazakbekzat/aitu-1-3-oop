package kz.aitu.oop.examples.Quiz6;

public class Singleton
{
    public String str="hello world";
    private static Singleton singleton = new Singleton();

    public Singleton()
    {

    }

    public static Singleton getSingleInstance()
    {
        return singleton;
    }
}