package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static java.lang.Integer.parseInt;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.*;
import java.util.stream.Collectors;

import kz.aitu.oop.repository.StudentFileRepository;

import static java.lang.Integer.parseInt;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {

    /**
     *
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */

    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        String result = "";
        String name = "";
        int groupName = parseInt(group.substring(2));
        for (Student student: studentFileRepository.getStudents()) {
            if (parseInt(student.getGroup().substring(2)) == groupName){
                name = student.getName();
                result = result+name+"\n";
            }
        }

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        StudentFileRepository studentFileRepository = new StudentFileRepository();
        String result = "";
        String name = "";
        int A = 0, B = 0, C = 0, D = 0, F = 0;
        int groupName = parseInt(group.substring(2));
        for (Student student: studentFileRepository.getStudents()) {
            if (parseInt(student.getGroup().substring(2)) == groupName){
                if(student.getPoint()>=90){
                    A++;
                }
                if(student.getPoint()<=89 && student.getPoint()>=80){
                    B++;
                }
                if(student.getPoint()<=79 && student.getPoint()>=70){
                    C++;
                }
                if(student.getPoint()<=69 && student.getPoint()>=60){
                    D++;
                }
                if(student.getPoint()<=59){
                    F++;
                }
            }
        }
        result= result+"A-"+A+","+"B-"+B+","+"C-"+C+","+"D-"+D+","+"F-"+F;

        return ResponseEntity.ok(result);
    }

    /**
     *
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {

        //write your code here
        String result = "";


        return ResponseEntity.ok(result);
    }
}
